# 360-3D VRPlayer #

### This is a 360-3D VR video(.mp4) player based on UtoVRPlayer.framework. 
Google card-board or equivalent card board style VR goggle is recommended.
###

## How to run ##
This project relies on cocoaPods.
https://guides.cocoapods.org/using/getting-started.html


## Some sample videos are within ##
* vowyr-ios / anylive / anylive / nature
* vowyr-ios / anylive / anylive / extreme
* vowyr-ios / anylive / anylive / beauty

## Refered libraries ##
* UtoVRPlayer
* FMMosaic
* MHYahoo
* MMParallax
* FMDB
* SDWebImage
* AWS(Legacy)

### This little project is a refurbish of a legacy project, some unwanted code maybe seen here and there. ###