//
//  DoLater.h
//  AdTest
//
//  Created by 孟 祥穎 on 2015/02/05.
//  Copyright (c) 2015年 cyberagent. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^laterBlock)();

@interface DoLater : NSObject

- (void)doLater:(laterBlock)block after:(NSTimeInterval)timeInterval;
- (void)cancel;

@end
