//
//  LiveItemView.m
//  anylive
//
//  Created by Teslac on 2015/09/19.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import "LiveItemView.h"

@implementation LiveItemView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initItem];
    }
    
    return self;
}

- (void)initItem{
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.text = @"Place Holder";
    label.center = self.center;
    [self addSubview:label];
    self.backgroundColor = [UIColor lightGrayColor];
}

@end
