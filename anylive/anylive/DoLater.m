//
//  DoLater.m
//  AdTest
//
//  Created by 孟 祥穎 on 2015/02/05.
//  Copyright (c) 2015年 cyberagent. All rights reserved.
//

#import "DoLater.h"

@implementation DoLater{
    NSTimer *theTimer;
    laterBlock b;
}

- (void)doLater:(laterBlock)block after:(NSTimeInterval)timeInterval{
    NSLog(@"Timer set.");
    b = block;
    theTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(myMethod) userInfo:nil repeats:NO];
}

- (void)cancel{
    if ([theTimer isValid])
    {
        
        NSLog(@"In-View timer canceled!");
        [theTimer invalidate];
        theTimer = nil;
    }
}

- (void)myMethod{
    b();
}

@end
