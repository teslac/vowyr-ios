//
//  PageViewController.m
//  MMParallaxScrollPresenter
//
//  Created by Malleo, Mitch on 12/19/14.
//

#import "PageViewController.h"
#import "LiveItemView.h"

#define ITEMNUMBER 5

@interface PageViewController ()


@end

@implementation PageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    for (int i = 0; i < ITEMNUMBER; i++) {
        LiveItemView* item1 = [[LiveItemView alloc] initWithFrame:CGRectMake(0, 0, screenWidth-40, 200)];
        
        item1.center = CGPointMake(screenWidth/2, 100+20+ 220*i);
        
        [self.scroll addSubview:item1];
    }
    
    
    
    
    
    
    CGSize s = CGSizeMake(screenWidth, 220*ITEMNUMBER+20);
    [self.scroll setContentSize:s];
    self.scroll.clipsToBounds = NO;
    self.scroll.scrollEnabled = NO;
    [self.view setFrame:CGRectMake(0, 0, s.width, s.height)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
