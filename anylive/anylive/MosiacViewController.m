//
//  MosiacViewController.m
//  anylive
//
//  Created by Teslac on 2015/09/27.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import "MosiacViewController.h"

#import "FMMosaicCellView.h"
#import "FMMosaicLayout.h"
#import "FMHeaderView.h"
#import "FMFooterView.h"


#import "DoLater.h"
#import <AWSDynamoDB.h>
#import "UIView+Toast.h"

#import "Live.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>

static const NSInteger kFMMosaicColumnCount = 1;

@interface MosiacViewController ()<FMMosaicLayoutDelegate>{
    NSArray*lives;
}



@end

@implementation MosiacViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    
    self.collectionView.bounces = NO;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.scrollEnabled = NO;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"FMHeaderView" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                 withReuseIdentifier:[FMHeaderView reuseIdentifier]];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"FMFooterView" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                 withReuseIdentifier:[FMFooterView reuseIdentifier]];
    
    [self adjustContentInsets];
//    [self doproductScan:NO];
    [self doVideoScan];
}


- (void)adjustContentInsets {
    UIEdgeInsets insets = UIEdgeInsetsMake([UIApplication sharedApplication].statusBarFrame.size.height, 0, 0, 0);
    self.collectionView.contentInset = insets;
    self.collectionView.scrollIndicatorInsets = insets;
}









- (void)doVideoScan{
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:self.genre];
    NSError * error;
    lives = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
    
    NSLog(@"directoryContents ====== %@",lives);
    [[self collectionView] reloadData];
    
    [[DoLater new] doLater:^{
        NSLog(@"content height: %f", self.collectionView.contentSize.height);
        //                     if (self.collectionView.contentSize.height == 0) {
        //                         [self.view setFrame:CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, 1000)];
        //                     }else
        [self.view setFrame:CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.collectionView.contentSize.height)];
        if (self.mmparallax) {
            [self.mmparallax refreshMMParallax];
        }
        
        if (self.topVC) {
            [self.topVC.tableView reloadData];
        }
        
    } after:0.1];
}




//[[NSBundle mainBundle] pathForResource:@"wu" ofType:@"mp4"]

- (void)doproductScan:(BOOL)isRefresh{
    //[self.sunnyRefreshControl startRefreshing];
    if (!isRefresh) {
        if (self.topVC) {
            [self.topVC startLoadingSign];
        }else
        [self.navigationController.view makeToastActivity];
    }
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    
    AWSDynamoDBScanExpression *scanExpression = [AWSDynamoDBScanExpression new];
    scanExpression.limit = @100;
    
    scanExpression.filterExpression = @"genre = :val";
    scanExpression.expressionAttributeValues = @{@":val":self.genre};
    
    
    [[dynamoDBObjectMapper scan:[Live class]
                     expression:scanExpression]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The request failed. Error: [%@]", task.error);
             [self.navigationController.view hideToastActivity];
             [self.navigationController.view makeToast:@"Fetch live failed." duration:0.5 position:CSToastPositionCenter title:nil image:[UIImage imageNamed:@"close"]];
         }
         if (task.exception) {
             NSLog(@"The request failed. Exception: [%@]", task.exception);
             [self.navigationController.view hideToastActivity];
             [self.navigationController.view makeToast:@"Fetch live failed." duration:0.5 position:CSToastPositionCenter title:nil image:[UIImage imageNamed:@"close"]];
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             
             
             
             lives = paginatedOutput.items;
             //NSLog(@"The request succeed: [%@]", lives);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (self.topVC) {
                     [self.topVC stopLoadingSign];
                 }else
                 [self.navigationController.view hideToastActivity];
                 
                 
                 
                 [[self collectionView] reloadData];
                 
                 [[DoLater new] doLater:^{
                     NSLog(@"content height: %f", self.collectionView.contentSize.height);
//                     if (self.collectionView.contentSize.height == 0) {
//                         [self.view setFrame:CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, 1000)];
//                     }else
                     [self.view setFrame:CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.collectionView.contentSize.height)];
                     if (self.mmparallax) {
                         [self.mmparallax refreshMMParallax];
                     }
                     
                     if (self.topVC) {
                         [self.topVC.tableView reloadData];
                     }
                     
                 } after:0.1];
                 
             });
             
         }
         return nil;
     }];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/








#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return lives.count;
}

- (NSString *)stringFromTimeInterval:(float)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (UIImage *)videoThumbNail:(NSURL *)videoURL
{
    UIImage *theImage = nil;
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(25, 1);
    CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
    
    theImage = [[UIImage alloc] initWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
    
    return theImage;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FMMosaicCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FMMosaicCellView reuseIdentifier] forIndexPath:indexPath];
    
    // Configure the cell
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:self.genre];
    documentsPath = [documentsPath stringByAppendingString:@"/"];
    cell.path = [documentsPath stringByAppendingString:(NSString*)[lives objectAtIndex:indexPath.row]];
    
    NSURL* videopath = [NSURL fileURLWithPath:cell.path];
    cell.imageView.image = [self videoThumbNail:videopath];
//    Live *prod = (Live *)[lives objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = [(NSString*)[lives objectAtIndex:indexPath.row] componentsSeparatedByString:@"."][0];//prod.title;
    cell.priceLabel.text = [(NSString*)[lives objectAtIndex:indexPath.row] componentsSeparatedByString:@"."][1];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videopath options:nil];
    CMTime audioDuration = asset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    cell.length.text = [self stringFromTimeInterval:audioDurationSeconds];
    
//    NSString*genrestring;
//    if ([prod.genre isEqualToString:GENRE_URBAN]) {
//        genrestring = @"URBAN";
//    }
//    
//    if ([prod.genre isEqualToString:GENRE_ROAD]) {
//        genrestring = @"ROAD";
//    }
//    
//    if ([prod.genre isEqualToString:GENRE_NATURE]) {
//        genrestring = @"NATURE";
//    }
//    
//    cell.priceLabel.text = [[prod.area stringByAppendingString:@" / "] stringByAppendingString:genrestring];
//    
//    if (indexPath.row % 2 == 0) {
//        cell.imageView.image = [UIImage imageNamed:@"gray1"];
//    }else{
//        cell.imageView.image = [UIImage imageNamed:@"gray2"];
//    }
//    
//    NSString* youtubeid = [prod.link componentsSeparatedByString:@"&"][0];
//    youtubeid = [youtubeid componentsSeparatedByString:@"="][1];
//    
//    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/hqdefault.jpg", youtubeid]] placeholderImage:[UIImage imageNamed:@"gray1"]];
    //http://img.youtube.com/vi/<insert-youtube-video-id-here>/mqdefault.jpg
    //http://img.youtube.com/vi/<insert-youtube-video-id-here>/maxresdefault.jpg
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        FMHeaderView *headerView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                           withReuseIdentifier:[FMHeaderView reuseIdentifier] forIndexPath:indexPath];
        switch (indexPath.section) {
            case 0:
                headerView.titleLabel.text = @"Hot Lives";
                break;
                
            default:
                break;
        }
        
        reusableView = headerView;
        
    }
    else if([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        FMFooterView *footerView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                           withReuseIdentifier:[FMFooterView reuseIdentifier] forIndexPath:indexPath];
        
        NSInteger assetCount = [self collectionView:self.collectionView numberOfItemsInSection:indexPath.section];
        footerView.titleLabel.text = assetCount == 1 ? @"共1件商品" : [NSString stringWithFormat:@"共%ld件商品", (long)assetCount];
        reusableView = footerView;
    }
    
    return reusableView;
}

#pragma mark <FMMosaicLayoutDelegate>

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
   numberOfColumnsInSection:(NSInteger)section {
    return kFMMosaicColumnCount;
}

- (FMMosaicCellSize)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
  mosaicCellSizeForItemAtIndexPath:(NSIndexPath *)indexPath {

//    if (indexPath.item == 5) {
//        return FMMosaicCellSizeSmall;
//    }
    return FMMosaicCellSizeBig;// (indexPath.item % 3 == 0) ? FMMosaicCellSizeSmall : FMMosaicCellSizeBig;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    if (self.topVC) {
        return UIEdgeInsetsMake(5.0, 0, 5.0, 0);
    }
    return UIEdgeInsetsMake(0.0, 0, 25.0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
interitemSpacingForSectionAtIndex:(NSInteger)section {
    return 15;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
 heightForHeaderInSection:(NSInteger)section {
    
    
    return 0.0;//kFMHeaderFooterHeight;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
 heightForFooterInSection:(NSInteger)section {
    return 0.0;// kFMHeaderFooterHeight;
}

- (BOOL)headerShouldOverlayContentInCollectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout {
    return YES;
}

- (BOOL)footerShouldOverlayContentInCollectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout {
    return YES;
}

#pragma mark - Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
