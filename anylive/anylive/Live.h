//
//  Live.h
//  anylive
//
//  Created by Teslac on 2015/09/26.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import "AWSDynamoDBObjectMapper.h"
#import <AWSDynamoDB.h>
@interface Live : AWSDynamoDBObjectModel<AWSDynamoDBModeling>

@property (nonatomic, strong) NSString *liveId;
@property (nonatomic, strong) NSString *link;

@property (nonatomic, strong) NSString *genre;
@property (nonatomic, strong) NSString *scenario;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *area;

@property (nonatomic, strong) NSString *dateTime;

@end
