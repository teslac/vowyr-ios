//
//  TopViewController.h
//  anylive
//
//  Created by Teslac on 2015/09/21.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHYahooParallaxView.h"

@interface TopViewController : UITableViewController<MHYahooParallaxViewDatasource, MHYahooParallaxViewDelegate>

- (void)startLoadingSign;
- (void)stopLoadingSign;

@end
