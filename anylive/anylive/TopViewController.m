//
//  TopViewController.m
//  anylive
//
//  Created by Teslac on 2015/09/21.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import "TopViewController.h"
#import "ViewController.h"
#import "MHYahooParallaxView.h"
#import "MHYahooWeatherParallaxCell.h"
//#import "Util.h"
#import "DoLater.h"
#import "MosiacViewController.h"

#include <stdlib.h>
#import "UIView+Toast.h"





#define IMAGE_SIZE_REDUCE_FACTOR 1.5

@interface TopViewController (){
    MHYahooParallaxView*slides;
    MosiacViewController* mosiacvc;
    int r;
    UIImageView* bgimg;
}

@property (weak, nonatomic) IBOutlet UIView *slidesContainer;

@property (weak, nonatomic) IBOutlet UIView *allLiveCell;
@property (weak, nonatomic) IBOutlet UILabel *searchByGenre;

@property (weak, nonatomic) IBOutlet UIImageView *searchBtn;

@end



@implementation TopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    slides = [[MHYahooParallaxView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, _slidesContainer.frame.size.height)];
    [slides registerClass:[MHYahooWeatherParallaxCell class] forCellWithReuseIdentifier:[MHYahooWeatherParallaxCell reuseIdentifier]];
    slides.delegate = self;
    slides.datasource = self;
    [_slidesContainer addSubview:slides];
    [slides startSlideTimer];
    
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    

    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18];
    title.textColor = [UIColor whiteColor];
    title.text = @"Anylive";
    [title sizeToFit];
    self.navigationItem.titleView = title;
    self.title = @"AnyLive";
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IMG_4309"]]];
    self.navigationItem.rightBarButtonItem = item;
    
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapped)];
    [self.searchByGenre addGestureRecognizer:recognizer];
    
    
    
    
    r = 2;
//    r = arc4random_uniform(3);
    NSString *rs = [NSString stringWithFormat:@"%d", r];
    UIImage *backgroundImage = [UIImage imageNamed:[@"bg" stringByAppendingString:rs]];
    bgimg = [[UIImageView alloc] initWithImage:backgroundImage];
    bgimg.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = bgimg;
    
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self
                                   selector:@selector(changebgimg) userInfo:nil repeats:YES];
    
    
    
    self.allLiveCell.bounds = CGRectMake(0, 0, self.allLiveCell.frame.size.width, 2000);
    
    
    mosiacvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MosiacViewController"];
    mosiacvc.genre = GENRE_BEAUTY;
    mosiacvc.topVC = self;
    [mosiacvc.view setFrame:CGRectMake(0, 0, self.allLiveCell.frame.size.width, self.allLiveCell.frame.size.height)];
    [self.allLiveCell addSubview:mosiacvc.view];
    [self addChildViewController:mosiacvc];
    [mosiacvc didMoveToParentViewController:self];

    //[mosiacvc.view layoutSubviews];
    
    
    
    
    
    
    self.searchBtn.layer.cornerRadius = 25;
    self.searchBtn.clipsToBounds = true;
    self.searchBtn.bounds = CGRectInset(self.searchBtn.frame, 20.0f, 20.0f);
}

-(void)onTapped{
    ViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:vc animated:YES];

}


-(void)changebgimg{
    r++;
    if (r == 3) {
        r = 0;
    }
    //[UIView animateWithDuration:0.5 animations:^{
        bgimg.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg%d", r]];
        CATransition *transition = [CATransition animation];
        transition.duration = 0.9f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        
        [bgimg.layer addAnimation:transition forKey:nil];
    //}];
}


- (void)startLoadingSign{
    [self.navigationController.view makeToastActivity];
}
- (void)stopLoadingSign{
    [self.navigationController.view hideToastActivity];
}



- (IBAction)refreshpulled:(id)sender {
    [self.refreshControl endRefreshing];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor grayColor];
    [cell setSelectedBackgroundView:bgColorView];

}
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"table select..");
    ViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    MosiacViewController* vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"MosiacViewController"];
    vc2.genre = GENRE_EXTREME;
    switch (indexPath.row) {
        case 2:
            [self.navigationController pushViewController:vc animated:YES];
            break;
        case 3:
            [self.navigationController pushViewController:vc2 animated:YES];
            break;
        default:
            break;
    }
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [[UIScreen mainScreen] bounds].size.height*0.618;
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        self.allLiveCell.frame = CGRectMake(self.allLiveCell.frame.origin.x, self.allLiveCell.frame.origin.y, self.allLiveCell.frame.size.width, mosiacvc.collectionView.contentSize.height);
        
        return 500+177+mosiacvc.collectionView.contentSize.height;
    }
    return 0;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UICollectionViewCell*) parallaxView:(MHYahooParallaxView *)parallaxView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MHYahooWeatherParallaxCell * ywCell = (MHYahooWeatherParallaxCell*)[parallaxView dequeueReusableCellWithReuseIdentifier:[MHYahooWeatherParallaxCell reuseIdentifier] forIndexPath:indexPath];
    
    UIImage * img = [UIImage imageNamed:[NSString stringWithFormat:@"%li",(long)indexPath.row]];
    
    ywCell.parallaxImageView.image = [TopViewController reduceImageSize:img];
    return ywCell;
}


- (NSInteger) numberOfRowsInParallaxView:(MHYahooParallaxView *)parallaxView {
    return 3;
}

- (void)parallaxViewDidScrollHorizontally:(MHYahooParallaxView *)parallaxView leftIndex:(NSInteger)leftIndex leftImageLeftMargin:(CGFloat)leftImageLeftMargin leftImageWidth:(CGFloat)leftImageWidth rightIndex:(NSInteger)rightIndex rightImageLeftMargin:(CGFloat)rightImageLeftMargin rightImageWidth:(CGFloat)rightImageWidth {
    
    // leftIndex and Right Index should must be greater than or equal to zero
    //NSLog(@"parallaxViewDidScrollHorizontally : %i, %i", leftIndex, rightIndex);
    
    if(leftIndex >= 0){
        MHYahooWeatherParallaxCell * leftCell = (MHYahooWeatherParallaxCell*)[parallaxView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:leftIndex inSection:0]];
        
        CGRect frame = leftCell.parallaxImageView.frame;
        frame.origin.x = leftImageLeftMargin;
        frame.size.width = leftImageWidth;
        leftCell.parallaxImageView.frame = frame;
    }
    if(rightIndex >= 0){
        MHYahooWeatherParallaxCell * rightCell = (MHYahooWeatherParallaxCell*)[parallaxView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:rightIndex inSection:0]];
        CGRect frame = rightCell.parallaxImageView.frame;
        frame.origin.x = rightImageLeftMargin;
        frame.size.width = rightImageWidth;
        rightCell.parallaxImageView.frame = frame;
    }
    
}


+ (UIImage*)reduceImageSize:(UIImage*)srcImg{
    CGSize size = [[UIScreen mainScreen] bounds].size;
    if (size.width * IMAGE_SIZE_REDUCE_FACTOR > srcImg.size.width || size.height * IMAGE_SIZE_REDUCE_FACTOR > srcImg.size.height) {
        return srcImg;
    }
    size.width *= IMAGE_SIZE_REDUCE_FACTOR;
    size.height *= IMAGE_SIZE_REDUCE_FACTOR;
    
    return [TopViewController resizeAspectFitWithSize:srcImg size:size];
}

+ (UIImage*)resizeAspectFitWithSize:(UIImage *)srcImg size:(CGSize)size {
    
    CGFloat widthRatio  = size.width  / srcImg.size.width;
    CGFloat heightRatio = size.height / srcImg.size.height;
    CGFloat ratio = (widthRatio < heightRatio) ? widthRatio : heightRatio;
    
    CGSize resizedSize = CGSizeMake(srcImg.size.width*ratio, srcImg.size.height*ratio);
    
    UIGraphicsBeginImageContext(resizedSize);
    [srcImg drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
    UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resizedImage;
}




@end
