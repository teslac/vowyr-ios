//
//  Live.m
//  anylive
//
//  Created by Teslac on 2015/09/26.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import "Live.h"

@implementation Live

+ (NSString *)dynamoDBTableName {
    return @"anylive-lives";
}

+ (NSString *)hashKeyAttribute {
    return @"liveId";
}

@end
