//
//  MosiacViewController.h
//  anylive
//
//  Created by Teslac on 2015/09/27.
//  Copyright © 2015年 troublemaker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "TopViewController.h"

#define GENRE_EXTREME @"extreme"
#define GENRE_BEAUTY @"beauty"
#define GENRE_NATURE @"nature"

@interface MosiacViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) NSString*genre;
@property (nonatomic) ViewController*mmparallax;
@property (nonatomic) TopViewController*topVC;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end
