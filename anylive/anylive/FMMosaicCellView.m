//
// FMMosaicCellView.m
// FMMosaicLayout
//
// Created by Julian Villella on 2015-01-30.
// Copyright (c) 2015 Fluid Media. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "FMMosaicCellView.h"
#import <UtoVRPlayer/UtoVRPlayer.h>
#import "PlayerViewController.h"
#import "AppDelegate.h"


static NSString* const kFMMosaicCellViewReuseIdentifier = @"FMMosaicCellViewReuseIdentifier";

@interface FMMosaicCellView () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *overlayView;

@end

@implementation FMMosaicCellView

+ (NSString *)reuseIdentifier {
    return kFMMosaicCellViewReuseIdentifier;
}

- (void)awakeFromNib {
    
    //[self.imageView setFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.width*9/16)];
    
    self.imageView.transform = CGAffineTransformMakeScale(2, 2);
    //    int r = 0.2;
    //    [self.imageView setFrame:CGRectMake(-self.imageView.frame.size.width*r, -self.imageView.frame.size.height*r, self.imageView.frame.size.width*(1+r), self.imageView.frame.size.height*(1+r))];
    
    self.imageView.transform = CGAffineTransformMakeRotation(-M_PI/35);
    //self.imageView.clipsToBounds = YES;
    
    self.avatarView.layer.cornerRadius = 30;
    self.avatarView.clipsToBounds = true;
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    longPressGesture.minimumPressDuration = 0.0;
    longPressGesture.delegate = self;
    longPressGesture.cancelsTouchesInView = NO;
    
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openDetail)];
    [self addGestureRecognizer:longPressGesture];
    [self addGestureRecognizer:tapGesture];
}

- (void)onLongPress:(UILongPressGestureRecognizer *)longPressGestureRecognizer {
    if (longPressGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [UIView animateWithDuration:0.2 delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.transform = CGAffineTransformMakeScale(0.98, 0.98);
            self.overlayView.alpha = 0.1;
        } completion:nil];
        
    } else if (longPressGestureRecognizer.state == UIGestureRecognizerStateEnded){
        [UIView animateWithDuration:0.2 delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.transform = CGAffineTransformIdentity;
            self.overlayView.alpha = 0.2;
        } completion:nil];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}










- (void)openDetail{
    //    [[[DoLater alloc] init] doLater:^{
    //        AdDetailAnimView *modalView = [[AdDetailAnimView alloc] initWithFrame:[Util getViewController].view.frame forAdControl:self.imageView selectedItem:_p];
    //        modalView.controller = [Util parentViewControllerOf:self];
    //        [[Util getViewController].view addSubview:modalView];
    //    } after:0.01];
    
    
    UVPlayerViewStyle style = UVPlayerViewStyleDefault;
    //    if (indexPath.section == 1 && indexPath.row == 1) {
    //        style = UVPlayerViewStyleNone;
    //    }
    
    
    PlayerViewController *playerVC = [[PlayerViewController alloc] init];
    
    NSMutableArray *items = [NSMutableArray array];
    
    //UVPlayerItem *item = [[UVPlayerItem alloc] initWithPath:@"http://resource.vr-store.cn/appfile/05c896d7dff047cca7568f892130a9e7.mp4" type:UVPlayerItemTypeOnline];
    UVPlayerItem *item = [[UVPlayerItem alloc] initWithPath:self.path type:UVPlayerItemTypeLocalVideo];
    
    [items addObject:item];
    
    [playerVC.itemsToPlay addObjectsFromArray:items];
    
    playerVC.player.viewStyle = style;
    playerVC.title = [NSString stringWithFormat:@"%@-%@",@"test", @"video"];
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [appDelegate.navictrlr pushViewController:playerVC animated:YES];
}






@end
