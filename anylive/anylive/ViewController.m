//
//  ViewController.m
//  anylive
//
//  Created by Teslac on 2015/09/19.
//  Copyright (c) 2015年 troublemaker. All rights reserved.
//

#import "ViewController.h"
#import "MMParallaxPage.h"
#import "MMParallaxPresenter.h"
#import "PageViewController.h"
#import "MosiacViewController.h"

#import "AppDelegate.h"

#define PARRALAX_HEADER_HEIGHT 120
#define PARRALAX_HEADER_MASK 0.2

@interface ViewController (){
    MMParallaxPage *page1, *page2, *page3;
}


@property (weak, nonatomic) IBOutlet MMParallaxPresenter *mmParallaxPresenter;

@end

@implementation ViewController


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    [self navigationController].hidesBarsOnSwipe = false;
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    appDelegate.navictrlr = self.navigationController;
    
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    
    // Do any additional setup after loading the view, typically from a nib.
    [self.mmParallaxPresenter setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    
    
    
    
    
    MosiacViewController *vc1 = [self.storyboard instantiateViewControllerWithIdentifier:@"MosiacViewController"];
    vc1.genre = GENRE_EXTREME;
    vc1.mmparallax = self;
    [self addChildViewController:vc1];
    [vc1 didMoveToParentViewController:self];
    
    MosiacViewController *vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"MosiacViewController"];
    vc2.genre = GENRE_BEAUTY;
    vc2.mmparallax = self;
    [self addChildViewController:vc2];
    [vc2 didMoveToParentViewController:self];
    
    MosiacViewController *vc3 = [self.storyboard instantiateViewControllerWithIdentifier:@"MosiacViewController"];
    vc3.genre = GENRE_NATURE;
    vc3.mmparallax = self;
    [self addChildViewController:vc3];
    [vc3 didMoveToParentViewController:self];
    
    
    
    page1 = [[MMParallaxPage alloc] initWithScrollFrame:self.mmParallaxPresenter.frame withHeaderHeight:PARRALAX_HEADER_HEIGHT andContentView:vc1.view];
    [page1.headerLabel setText:@"极限运动"];
    UIImageView* tmp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"extreme"]];
    tmp.contentMode = UIViewContentModeScaleAspectFill;
    ///
    UIView *blackMask = [[UIView alloc] initWithFrame:tmp.frame];
    blackMask.backgroundColor = [UIColor blackColor];
    blackMask.alpha = PARRALAX_HEADER_MASK;
    [tmp addSubview:blackMask];
    ///
    [page1.headerView addSubview:tmp];
    
    page2 = [[MMParallaxPage alloc] initWithScrollFrame:self.mmParallaxPresenter.frame withHeaderHeight:PARRALAX_HEADER_HEIGHT andContentView:vc2.view];
    [page2.headerLabel setText:@"美女野兽"];
    tmp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bl"]];
    tmp.contentMode = UIViewContentModeScaleAspectFill;
    ///
    UIView *blackMask2 = [[UIView alloc] initWithFrame:tmp.frame];
    blackMask2.backgroundColor = [UIColor blackColor];
    blackMask2.alpha = PARRALAX_HEADER_MASK;
    [tmp addSubview:blackMask2];
    ///
    [page2.headerView addSubview:tmp];
    
    
    page3 = [[MMParallaxPage alloc] initWithScrollFrame:self.mmParallaxPresenter.frame withHeaderHeight:PARRALAX_HEADER_HEIGHT andContentView:vc3.view];
    [page3.headerLabel setText:@"游戏电影"];
    [page3 setTitleAlignment:MMParallaxPageTitleBottomLeftAlignment];
    tmp = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"valkyrine"]];
    tmp.contentMode = UIViewContentModeScaleAspectFill;
    ///
    UIView *blackMask3 = [[UIView alloc] initWithFrame:tmp.frame];
    blackMask3.backgroundColor = [UIColor blackColor];
    blackMask3.alpha = PARRALAX_HEADER_MASK;
    [tmp addSubview:blackMask3];
    ///
    [page3.headerView addSubview:tmp];
    
    [self.mmParallaxPresenter addParallaxPageArray:@[page1, page2, page3]];
}

- (void)refreshMMParallax{
    [self.mmParallaxPresenter reset];
    [self.mmParallaxPresenter addParallaxPageArray:@[page1, page2, page3]];
}

- (NSString *)sampleText
{
    return @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proideLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint ehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n \n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
